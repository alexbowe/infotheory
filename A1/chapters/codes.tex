\item
\begin{mydef}[Instantaneous Code]
    A code is considered \emph{instantaneous} or a \emph{prefix code} if no codeword is a prefix of another codeword.
    This allows a codeword to be decoded as soon as it is transmitted, whereas a non-instantaneous code may have to wait
    to see future codewords in order to determine the context. This wait may be one bit, or it may be until the end of the
    message. In this sense, it is self-punctuating.

    An example of a non-instantaneous code might have codewords $3 \rightarrow 11$ and $4 \rightarrow 110$, which means a decoder
    would need to see the following bit after receiving $11$ before it can decode to either $3$ or $4$.
\end{mydef}


\item
A tight upper and lower bound on the expected length $L$ of an instantaneous code is given by
\[
H(X) \le L < H(X) + 1   \qquad\text{\emph{(Cover and Thomas, 5.27)}}
\]


\item
An optimal prefix code can be obtained using Huffman's algorithm.

Since $X$ is uniformly distributed, $p(x) = \frac{1}{11} \forall x \in \mathcal{X}$.
\\
% Set the overall layout of the tree
\tikzstyle{level 1}=[level distance=2cm, sibling distance=5cm]
\tikzstyle{level 2}=[level distance=2cm, sibling distance=2cm]
\tikzstyle{level 3}=[level distance=2cm, sibling distance=1cm]
\tikzstyle{level 4}=[level distance=2cm, sibling distance=.5cm]

% Define styles for bags and leafs
%\tikzstyle{bag} = [text width=4em, text centered]
%\tikzstyle{end} = [circle, minimum width=3pt,fill, inner sep=0pt]

% The sloped option gives rotated edge labels. Personally
% I find sloped labels a bit difficult to read. Remove the sloped options
% to get horizontal labels. 
\begin{tikzpicture}[grow=right, sloped]
\node {$1$}
    child {
        node {$\frac{6}{11}$}
        child {
            node {$\frac{4}{11}$}
            child {
                node {$\frac{2}{11}$}
                child {
                    node {$\frac{1}{11}$}
                    edge from parent
                    node[below]{1}
                }
                child {
                    node {$\frac{1}{11}$}
                    edge from parent
                    node[above]{0}
                }
                edge from parent
                node[below]{1}
            }
            child {
                node {$\frac{2}{11}$}
                child {
                    node {$\frac{1}{11}$}
                    edge from parent
                    node[below]{1}
                }
                child {
                    node {$\frac{1}{11}$}
                    edge from parent
                    node[above]{0}
                }
                edge from parent
                node[above]{0}
            }
            edge from parent
            node[below]{1}
        }
        child {
            node {$\frac{2}{11}$}
            child {
                node {$\frac{1}{11}$}
                edge from parent
                node[below]{1}
            }
            child {
                node {$\frac{1}{11}$}
                edge from parent
                node[above]{0}
            }
            edge from parent
            node[above]{0}
        }
        edge from parent
        node[below]{1}
    }
    child {
        node {$\frac{5}{11}$}
        child {
            node {$\frac{3}{11}$}
            child {
                node {$\frac{2}{11}$}
                child {
                    node {$\frac{1}{11}$}
                    edge from parent
                    node[below]{1}
                }
                child {
                    node {$\frac{1}{11}$}
                    edge from parent
                    node[above]{0}
                }
                edge from parent
                node[below]{1}
            }
            child {
                node {$\frac{1}{11}$}
                edge from parent
                node[above]{0}
            }
            edge from parent
            node[below]{1}
        }
        child {
            node {$\frac{2}{11}$}
            child {
                node {$\frac{1}{11}$}
                edge from parent
                node[below]{1}
            }
            child {
                node {$\frac{1}{11}$}
                edge from parent
                node[above]{0}
            }
            edge from parent
            node[above]{0}
        }
        edge from parent
        node[above]{0}
    };
\end{tikzpicture}

We can then traverse the tree and assign each codeword to any symbol (since they are all equally likely).
The resulting code is shown in the Table \ref{tab:code} below.

\begin{table}[h]
\begin{center}
\begin{tabular}{ccl}
\toprule
X & Codeword Length & Codeword \\
\midrule
0 & 3 & 000 \\
1 & 3 & 001\\
2 & 3 & 010\\
3 & 4 & 1100\\
4 & 3 & 100\\
5 & 3 & 101\\
6 & 4 & 0110\\
7 & 4 & 0111\\
8 & 4 & 1101\\
8 & 4 & 1110\\
10 & 4 & 1111\\
\bottomrule
\end{tabular}
\caption{A Huffman Code (and hence optimal prefix code) for $X$ uniformly distributed over $\mathcal{X}$.}
\label{tab:code}
\end{center}
\end{table}

Thus, the expected codeword length $L$ is

\begin{align*}
    L &= E[l(x)]\\
      &= \mysum{x \in \mathcal{X}} p(x) l(x)\\
      &= \frac{5 \cdot 3 + 6 \cdot 4}{11}\\
      &\approx 3.54545 \text{ bits}
\end{align*}

And the entropy $H(X)$ is

\begin{align*}
    H(X) &= -\mysum{x in \mathcal{X}} p(x) \log p(x) \\
         &= \log 11 \\
         &\approx 3.45943 \text{ bits}
\end{align*}

So the expected codeword length is close to $0.08602$ of a bit greater than the entropy $H(X)$.


\item
\begin{itemize}
\item[i.]

%((the length $l_i = \log 1/p_i$ according to shannon-fano? Try that decimal argument))

Since for each $x \in \mathcal{X}$ the probability $p(x) = \frac{1}{N}$, where $N$ is not a power of $2$, then
the codeword length will either be $l_N$ or $l_N - 1$ bits (for example, the prefix tree would be complete to the $(N - 1)^{th}$ level).

These two possible codeword lengths mean we have $aN$ codewords of length $l_N$ and $(1 - a)N$ of length $l_N - 1$.
The code is optimal, so the Kraft inequality must hold:

\begin{align*}
   & & \mysum{i=1}^{N} 2^{-l_i}& &\le& 1 \\
\therefore& &aN2^{-l_N} + N(1-a)2^{-l_N + 1}& &\le& 1 \\
\therefore& &a2^{-l_N} + 2(1-a)2^{-l_N}& &\le& \frac{1}{N} \\
\therefore& &2^{-l_N} (a + 2 - 2a)& &\le& \frac{1}{N} \\
\therefore& &2 - a& &\le& \frac{2^{-l_N}}{N} \\
\therefore& &a& &\ge& 2 - \frac{2^{l_N}}{N}
\end{align*}

Since $a$ is a fraction codewords of $N$, $a \le 1$. Also, $\frac{2^{l_N}}{N} = 2^{\lceil{\log N}\rceil - \log N}$, and is hence bound between $0$ and $1$ (an observation: this represents the fraction of a bit we are wasting on this description). This is a tight bound, so we can approximate it as $a = 2 - \frac{2^{l_N}}{N}$. 

\item[ii.]
\begin{align*}
    L &= \mysum{i = 1}^{N} p_i l_i \\
      &= Na (\frac{1}{N} l_N) + N(1-a) [\frac{1}{N} (l_N - 1)] \\
      &= a l_N + (1 - a) (l_N - 1) \\
      &= a l_N - a l_N + l_N + a - 1 \\
      &= l_N + a - 1 \qed
\end{align*}

\item[iii.]
The excess length,
\begin{align*}
    \Delta L &= L - H(X) \\
             &= l_N + a - 1 + \mysum{x \in \mathcal{X}} p(x) \log p(x) \\
             &= l_N + a - 1 - N \frac{1}{N} \log \frac{1}{N} \\
             &= l_N + \left(2 - \frac{2^{l_N}}{N}\right) - 1 - \log N \\
             &= \lceil \log (2^k + d) \rceil +  \left(2 - \frac{2^{\lceil2^k + z\rceil}}{N}\right) - 1 - \log (2^k + d)\\
             &= \lfloor \log (2^k + d) \rfloor + 1 + \left(2 - \frac{2^{\lceil2^k + z\rceil}}{N}\right) - 1 - \log (2^k + d) \\
             &= k + \left(2 - \frac{2^{\lceil2^k + d\rceil - \log N}}{N}\right) - \log (2^k + d) \\
             &= k + \frac{2d}{N} - \log(2^k + d)
\end{align*}

for $N = 2^k + d$, and $d$ the difference between $N$ and next smallest power of 2; $d = N -2^{\lfloor \log N \lfloor}$
Deriving this with respect to $d$:

\[
    \frac{\delta \Delta L}{\delta d} = \frac{(2^k + d)(2) - 2d}{2^k + d)^2} - \frac{1}{\ln 2}\cdot\frac{1}{2^k + d}
\]

I have had trouble solving this for zero (within time constraints of the deadline),
but I used Wolfram Alpha to solve it for me, and it gives $0.08602$.

The maximum given in the question is $1 - \frac{\ln \ln 2}{\ln 2} - \frac{1}{\ln 2}$,
which is about $0.08607$, which is greater than $0.08602$.

\end{itemize}
