\item
The terms ``$\epsilon$-typical'' and jointly ``$\epsilon$-typical''
will be defined shortly. First I will define the weak law of large numbers, and 
an analog of the \emph{weak law of large numbers}, called
the Asymptotic Equipartition Property (AEP).

\begin{mydef}[Weak Law of Large Numbers]
    The weak law of large numbers states that the sample mean of an independent, identically distributed
    (i.i.d.) random variables $X_i, i=1,2,...,n, ~  p_X(x)$, $\frac{1}{n} \mysum{i=1}^{n}X_i$, converges in probability to the expected value
    (or population mean) $E_{p_X}[X]$ for large values of $n$, that is

    \[
        \lim_{n \to \infty} Pr\{|X_n - X| > \epsilon \} \to 0, \forall \epsilon > 0.
    \]
\end{mydef}

\begin{mydef}[Asymptotic Equipartition Property (AEP)]
    The Asymptotic Equipartition Property (AEP) states that the empirical entropy of i.i.d random variables $X_1, X_2, ..., X_n$
    converges in probability to the actual entropy,

    \[ \lim_{n \to \infty} Pr\{|-\frac{1}{n}\log p(X_1, X_2, ..., X_n) - H(X)| > \epsilon\} \to 0, \forall \epsilon > 0,\]
    
    which is to say, it becomes less likely that the empirical entropy will differ by more than $\epsilon$ from the actual entropy.
\end{mydef}

This gives us the background knowledge required to explain ``$\epsilon$-typical'' sequences.

\begin{mydef}[$\epsilon$-Typical]
    A sequence $\textbf{x}^n = (x_1, x_2, ..., x_n)$ is $\epsilon$-typical 
    if the empirical entropy is within $\epsilon$ of the actual entropy,
    
    \[
        |-\frac{1}{n} \log p(\textbf{x}^n) - H(X)| \le \epsilon,
    \]
    
    The set of all such vectors is called the \emph{typical set}, denoted by $A_{\epsilon}^{(n)}$.
\end{mydef}

Finally, we can define ``jointly $\epsilon$-typical'' sequences.

\begin{mydef}[Jointly $\epsilon$-Typical]
    Two sequences $\textbf{x}^n$ and $\textbf{y}^n$ are jointly $\epsilon$-typical if
    they are both individually $\epsilon$-typical, and the joint empirical entropy is $\epsilon$-close to $H(X,Y)$,
    that is

    \begin{itemize}
        \item $|-\frac{1}{n} \log p_X(\textbf{x}^n) - H(X)| \le \epsilon$
        \item $|-\frac{1}{n} \log p_Y(\textbf{y}^n) - H(Y)| \le \epsilon$
        \item $|-\frac{1}{n} \log p(\textbf{x}^n, \textbf{y}^n) - H(X, Y)| \le \epsilon$
    \end{itemize}
    
    where $p(\textbf{x}^n, \textbf{y}^n) = \displaystyle\prod\limits_{i = 1}^{n} p(x_i, y_i)$.
    The set of all such pairs $(\textbf{x}^n, \textbf{y}^n) \in \mathcal{X}^n \times \mathcal{Y}^n$ is also represented as
    $A_{\epsilon}^{(n)}$. The context should be enough to disambiguate this, but if it is not I will write
    $A_{\epsilon}^{(n,n)}$ to mean the jointly typical set, and ${}^xA_{\epsilon}^{(n)}$ and ${}^yA_{\epsilon}^{(n)}$ to represent the typical set with respect to $p_X(x)$ and $p_Y(y)$ respectively.
\end{mydef}


\item
Typical set decoding can be demonstrated by this sequence of events:

\begin{enumerate}
    \item \emph{(Encoding)} Sender and receiver have an agreed upon (randomly generated)
          codebook $\mathcal{C}$, which has a codeword $\textbf{x}^n(w)$ for each message $w \in W = 1, 2, ..., M$
    \item
        Receiver receives a sequence $\textbf{Y}^n$, with known transission matrix
        $p(\textbf{y}^n | \textbf{x}^n(w))$
    \item 
        Receiver searches in $\mathcal{C}$ to find all codewords which are jointly typical with 
        $\textbf{Y}^n$.
    \item
        If there is zero or more than one result, then there is some ambiguity and the error can't be corrected.
        Set $\hat{W} = 0$.
    \item
        If there is only one, $x^n(w)$, then we assume the decoded message $\hat{W} = w$. This may not necessarily
        be the original message $W$, but this error cannot be detected without feedback.
\end{enumerate}


\item
Since the definition of joint $\epsilon$-typicality is

$H(X,Y) + \epsilon \le  -\frac{1}{n} \log p(\textbf{x}^n, \textbf{y}^n) \le H(X,Y) - \epsilon$,
then the probability can be given by multiplying by $-n$ and raising as an index of $2$:

$2^{-n(H(X,Y) + \epsilon)} \ge p(\textbf{x}^n, \textbf{y}^n) \ge 2^{-n(H(X,Y) - \epsilon}$.

The number of jointly $\epsilon$-typical pairs in $A_{\epsilon}^{(n)}$ can be solved like so:

\begin{align*}
    1 &= \mysum{(\textbf{x}^n,\textbf{y}^n) \in \mathcal{X}^n \times \mathcal{Y}^n} p(\textbf{x}^n, \textbf{y}^n) \\
      &\ge \mysum{(\textbf{x}^n, \textbf{y}^n)} p(\textbf{x}^n, \textbf{y}^n) \\
      &= |A_{\epsilon}^{(n)}| 2^{-n(H(X,Y) + \epsilon}
\end{align*}

Hence, $ |A_{\epsilon}^{(n)}| \le  2^{n(H(X,Y) + \epsilon}$. A similar argument can be made for the lower bound 
 $|A_{\epsilon}^{(n)}| \ge (1-\epsilon)2^{n(H(X,Y) - \epsilon}$, so I would approximate the number of jointly $\epsilon$-typical pairs as $2^{nH(X,Y)}$ (as $\epsilon$ can be made arbitrarily small for large enough $n$).


\item
Using similar arguments to the above question, the number of $\epsilon$-typical received sequences $\textbf{y}^n$ can be approximated by $2^{nH(Y)}$. This is more than the number of $\epsilon$-typical sequences in the the jointly typical set, but this is an acceptable answer if we consider the possibility that the received sequence is not in the codebook (a possible error).


\item
The average number of jointly $\epsilon$-typical source vectors, given a received vector, is about $2^{nH(X|Y)}$.

\item
There are about $2^{nH(X)}$ possible $\epsilon$-typical source vectors, and about $2^{nH(X|Y)}$ of them are jointly $\epsilon$-typical, which means the probability of finding such a source vector is approximately $\frac{2^{nH(X|Y)}}{2^{nH(X)}} = 2^{-nI(X;Y})$.

\item
I'll assume ``wrong'' to mean jointly $\epsilon$-typical with the received vector $\textbf{y}^n$ (it could also mean non-jointly typical sequences, which are ``very wrong'', but the probability of errors we can't detect is more interesting). I'll also assume the codebook is uniquely decodable (which isn't necessarily the case, depending on the way it is randomly generated).

This error will be detected if there is more than 1.

I have run out of time to complete this question and the next one.

\item

Ran out of time...
