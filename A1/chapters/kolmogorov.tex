\item
Let $\textbf{x}^n = (x_1, x_2, ..., x_n)$ (occasionally denoted as just \textbf{x}) represent a finite-length binary
string of length $l(\textbf{x}^n) = n$. Let $\mathcal{U}$ be a universal computer whose output of a program $p$ is
denoted as $\mathcal{U}(p)$.

\begin{mydef}[Kolmogorov Complexity]
The \emph{Kolmogorov Complexity} $K_\mathcal{U}(\textbf{x}^n)$ of a string $\textbf{x}^n$ with respect to a universal
computer $\mathcal{U}$ is defined as the shortest program string that $\mathcal{U}$ accepts that prints $\textbf{x}^n$:

\[
    K_{\mathcal{U}}(\textbf{x}^n) = \displaystyle\min\limits_{p:\mathcal{U}(p)=\textbf{x}} l(p)
\]

For any other computer $\mathcal{A}$ there exists a constant $c_{\mathcal{A}}$ such that

\[
    K_{\mathcal{U}}(\textbf{x}^n) \le K_{\mathcal{A}}(\textbf{x}^n) + c_{\mathcal{A}}
\]

where the constant $c_{\mathcal{A}}$ doesn't depend on $\textbf{x}^n$, and represents the length of a $\mathcal{U}$-program that 
interprets $\mathcal{A}$-programs to $\mathcal{U}$-programs. Since this is just a constant difference, it becomes negligable as the length
of $\textbf{x}$ increases. It is common to drop the $\mathcal{U}$ subscript, and refer to Kolmogorov Complexity as just $K(\textbf{x}^n)$,
assuming a fixed universal computer $\mathcal{U}$.
\end{mydef}

If the length $l(\textbf{x})$ is known, then we consider the conditional Kolmogorov Complexity:

\begin{mydef}[Conditional Kolmogorov Complexity]
Conditional Kolmogorov Complexity is the minimum program length to print a sequence $\textbf{x}$ when the length is known at the time of
generating the program. Hence, the length $n$ can be inherent in the procedures, and need not be encoded in a general way.
\[
    K(\textbf{x}^n|n) \le n + c,
\]
that is, the length of the program is at most a constant-sized program for printing $n$ bits, and the $n$-length
bitstring $\textbf{x}$ itself.
\end{mydef}

Conditional Kolmogorov Complexity is worth considering because it allows us to ignore the additional bits required to delimit the program,
if we know the length of the output string.
It also gives us an upper bound on unconditional Kolmogorov Complexity.

One delimitation technique is to encode each bit of $n$ as itself repeated twice, then terminate the sequence with $01$.
Another is to store the length $l(\textbf{x}) = n$ in $\log n$ bits, but we will then need to know how many bits
$\log n$ is in order to decode it. We can store this number in $\log \log n$ bits, but are faced with the same problem.
If we keep applying this recursively, we get $\log^{*} n = \log n + \log \log n + \log \log \log n ...$ bits, where the series terminates
at the last positive term.

These two encoding schemes give us the following upper bounds, respectively:

\begin{align}
K(\textbf{x}^n) &\le K(\textbf{x}^n|n) + 2 \log n + c \\
K(\textbf{x}^n) &\le K(\textbf{x}^n|n) + \log^{*} n + c
\end{align}


\item
\begin{itemize}[itemsep=20pt,parsep=2pt]
\item[i.]
\textbf{Theorem:}
\[
K(\textbf{y}^n|n) \le n + c
\]

\textbf{Proof:}
To print $n$ symbols of a sequence $\textbf{y}$ in memory, we only need a procedure that moves to the location of $y$, and
to loop across the sequence $n$ times. This corresponds to the constant $c$ `quads' (elements of quaternery alphabet $\mathcal{Y}$).
We know how many bits $n$ takes up, so the procedure can be written with the length independent of $n$.

Next, the number of quads to actually store the sequence $y$ is $n$. The resulting program would have the literal $y$ embedded in it,
and a procedure that always prints $n$ characters. So if $p$ denotes this program, and $P_n$ is a procedure to print $n$ symbols from
a given point in memory, then the length of the program is

\[
    l(p) = l(P_n) + l(\textbf{x}^n) = c + n
\]

Note that this is an upper bound. If $y$ has some exploitable pattern the inequality may be achieved. A trivial example of this would be
if $y = 0^n$ ($0$ repeated $n$ times), so then only a single $0$ would need to be stored instead of the entire sequence (or similarly,
to generate the sequence, such as digits of $\pi$). \qed

\item[ii.]
\textbf{Theorem:}
\[
    |\{\textbf{y}^n \in \mathcal{Y}^* : K(\textbf{y}^n|n) < k \}| < 4^k
\]

\textbf{Proof:}
Let $S$ denote the sum of a geometric progression:
\begin{align*}
          &   &S =& r^0 + r^1 + r^2 + ... + r^n\\
\therefore& &rS =& r^1 + r^2 + ... + r^{n+1} \\
          &  & =& S - r^0 + r^{n+1} \\
\therefore& &S - rS =& r^0 - r^{n+1} \\
\therefore&  &S =& \frac{1 - r^{n+1}}{1-r}
\end{align*}

Let $T$ denote the set of quaternary program strings with length less than $k$, then using the above
derivation for geometric series:

\begin{align*}
    |T| &= \mysum{i=0}^{k-1} 4^i \\
        &= 1 + 4 + 16 + ... + 4^{k-1} \\
        &= \frac{4^k - 1}{3} \\
        &< 4^k \qed 
\end{align*} 
\end{itemize}

\item \begin{mydef}[Algorithmically Random]
A sequence $\textbf{x}^n$ is said to be \emph{algorithmically random} if
\[
    K(\textbf{x}^n|n) \ge n
\]
\end{mydef}


\item As discussed in the lecture, one way of creating a program to print a
string $\textbf{x}$ is to use some ordered library of strings as a reference,
and print the $i^{th}$ entry (corresponding to $\textbf{x}$, where $i$ is an
internal parameter.

In particular, we can generate all sequences of length $n$, having $k_0$ $0$s,
$k_1$ $1$s, $k_2$ $2$s, and $k_3$ $3$s. A procedure that generates all such
permutations can be written in a constant amount of characters. Then, the
length of the program $p$ can be reduced to some constant sized string, and the
number of symbols required to specify the length $n$, and $k$ values:

\[
    l(p) = c + \log_4 n + \log_4 |T(P)|,
\]

where $T(P)$ is the \emph{type class} of $\textbf{y}$, which is the collection of
all sequences having the same distribution (or type) $P$ for $\mathcal{Y}$,
$P = \{\frac{k_0}{n}, \frac{k_1}{n}, \frac{k_2}{n}, \frac{k_3}{n} \}$. Then the size of the type class
can be calculated exactly as $|T(P)| = {n \choose k_0,k_1,k_2,k_3}$. This is a difficult equation to
manipulate, hence we use an upper bound $|T(P)| \le 2^{nH_2(P)}$, proved below.

A type class $T(P)$ must have probability $p(T(P)) \le 1$,
\begin{align*}
    1 &\ge p(T(P)) \\
      &= \mysum{\textbf{x} \in T(P)} p(\textbf{x})\\
      &= \mysum{\textbf{x} \in T(P)} 2^{-nH_2(P)}\\
      &= |T(P)|2^{-nH_2(P)}
\end{align*}
Thus,
\[
    |T(P)| \le 2^{nH_2(P)}
\]

Although not necessary, to reduce confusion of changing logarithm bases 
this result can be related to quaternary entropy:

\begin{align*}
    2^{nH_2(P)} &= 2^{n \log_2 4 H_4(P)} \qquad\qquad(H_b(X) = (\log_b a) H_a(X)) \\
                &= 4^{nH_4(P)}
\end{align*}

This gives us the upper bound

\begin{align*}
    l(p) &\le c + \log_4 n + \log_4 4^{nH_4(P)} \\
         &= c + \log_4 n + nH_4(P) \log_4 4 \\
         &= c + \log_4 n + nH_4(P)
\end{align*}

Given that $\textbf{y}$ is algorithmically random, we know a lower bound for the program too

\[
    n \le K(\textbf{y}^n|n) \le c + \log_4 n + nH_4(P)
\]

Thus,

\[
    H_4(P) \le 1 - \frac{c + \log_4 n}{n}
\]

Since $\log_4 n \in o(n)$ and $c \in o(n)$ (by defintion), RHS $\rightarrow 1$ as
$n$ grows larger. $H_4(P) \le \log_4 |\mathcal{Y}| = 1$ with equality iff each symbol is uniformly distributed.
Hence, by the Strong Law of Large Numbers, $P$ is converging to have proportions $\frac{1}{4}$ for each
$y \in \mathcal{Y}$.
