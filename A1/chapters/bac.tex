\item
A Discrete Memoryless Channel (DMC) is defined as such

\begin{mydef}[Discrete Memoryless Channel]
A discrete channel consists of an input alphabet $\mathcal{X}$ and output alphabet
$\mathcal{Y}$, and a $|\mathcal{Y}|\times|\mathcal{X}|$ matrix of conditional probabilities $p(y|x)$.

A discrete channel is \emph{memoryless} if input $X_t$ is not dependent on previous inputs and outputs
$X_{t - k}$ or $Y_{t - k} \forall k = 1, 2, 3...$ (it can not `remember' previous transmissions or outcomes).
\end{mydef}

In this question, the input and output alphabets are binary. It is not stated explicitly,
but since it discusses addition modulo 2 we can assume $\mathcal{X} = \mathcal{Y} = \{0, 1\}$.

As $Y_i = X_i + Z_i \mod{2}$, the matrix $p(y|x)$ is defined by the random variable $Z_i$, which represents a bit error:

\[ p(y|x) = \left[ \begin{array}{cc}
1-p & p \\
p & 1-p \end{array} \right]\] 

A diagram of this channel can be seen in Figure \ref{fig:dmc}.

\DefFig{fig:dmc}{bac/channel}{0.3}
        {Channel diagram for the binary channel $Y_i = X_i + Z_i \mod{2}$ with $p = Pr\{Z_i = 1\}$.}


\item
The equation $D = 1 - H_0(p)$ represents the information capacity of the channel for a single use, or the highest possible mutual information
between the received $Y$ and the sent $X$ (and hence, how much we can recover). Intuitively, since we are transmitting $1$ bit
per use with the probability $p$ of a bit-flip, we can say that the mutual information must be $1$ bit minus some uncertainty.
This is proved below:

\begin{align*}
I(X;Y) &= H(Y) - H(Y|X) \\
       &= H(Y) - \mysum{x \in \mathcal{X}} p_X(x) H(Y|X=x) \\
       &= H(Y) - \mysum{x \in \mathcal{X}} p_X(x) (- p \log p - (1-p) log (1-p)) \\
       &= H(Y) - \mysum{x \in \mathcal{X}} p_X(x) H_0(p) \\
       &= H(Y) - H_0(p)
\end{align*}

So, the information capacity $D$ (which maximises $I(X;Y)$ with respect to $p_X$), is calculated as follows:

Let $Pr\{X=1\} = \pi$, then $p_Y(1) = \pi p(y|x = 1) = \pi(1 - p + p) = \pi$ and $p_Y(1) = 1 - \pi$.

\begin{align*}
    D &= \displaystyle\max\limits_{p_X(x)} (H(Y) - H_0(p)) \\
      &= H_0(\pi) - H_0(p) \\
      &= 1 - H_0(p) \qquad\text{when $\pi = \frac{1}{2}$} \qed 
\end{align*}


\item
Now $Y_i = Z_i + X_i$ but $Z_1, Z_2, ..., Z_n$ aren't necessarily independent.

Using

\begin{align*}
    I(\textbf{X}^n;\textbf{Y}^n) &= H(\textbf{X}^n) - H(\textbf{X}^n|\textbf{Y}^n) \\
                                 &= H(\textbf{X}^n) - H(\textbf{Z}^n|\textbf{Y}^n) \\
                                 &\ge H(\textbf{X}^n) - H(\textbf{Z}^n) \\
                                 &\ge H(\textbf{X}^n) - \mysum{i = 1}^{n} H(Z_i) \\
                                 &= H(\textbf{X}^n) - nH(p) \\
                                 &= n - nH(p),
\end{align*}

if we choose $X_1, X_2, ..., X_n$ i.i.d. distributed as a Bernoulli random variable
with paramater $0.5$.

So, the capacity over $n$ uses is $nC \ge n - nH(p)$, hence $C \ge 1 - H(p) = D$.

This just means that since $Z_i$ aren't necessarily independent, we could potentially
use statistics on previous $Z_i$ to predict the future noise, and try to combat it.
