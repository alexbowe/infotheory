\item


\DefFig{fig:mutual}{entropy/mutual}{0.4}
        {Venn diagram demonstrating the connection of mutual entropy to conditional, marginal and joint entropy.}

The \emph{Mutual Information} $I(X; Y)$ is the reduction of uncertainty
in $X$ when $Y$ is known. It is defined mathematically as follows:


\begin{mydef}[Mutual Information]
\begin{align*}
    I(X; Y) &= \displaystyle\sum\limits_{(x,y) \in \mathcal{X} \otimes \mathcal{Y}}
              p(x, y)\log \frac{p(x, y)}{p_X(x)p_Y(y)}\\
            &= E_{p(x,y)} \log \frac{p(X,Y)}{p_X(X)p_Y(Y)}
\end{align*}
\end{mydef}


This definition can be related back to entropy:
\begin{align}
    I(X; Y) &= E_{p(x,y)} \log \frac{p(X,Y)}{p_X(X)p_Y(Y)} \notag \\
            &= E_{p(x,y)} \log \frac{p(X|Y)}{p_X(X)} \notag \\
            &= E_{p(x,y)} \log p(X|Y) - E_{p(x,y)} \log p_X(X) \notag \\
            &= - E_{p(x,y)} \log p_X(X) -
               \left( - E_{p(x,y)} \log p(X|Y) \right) \notag \\
            &= H(X) - H(X|Y) \label{eq:mutual1}
\end{align}

By symmetry it can be shown that:
\begin{align}
    I(X; Y) &= H(Y) - H(Y|X)\label{eq:mutual2}
\end{align}



Additionally, from the chain rule for entropy, it is known that:
\begin{align}
            H(X,Y) &= H(X) + H(Y|X) \notag \\
\therefore  H(Y|X) &= H(X,Y) - H(X) \label{eq:chain}
\end{align}

It follows from \eqref{eq:mutual2} and \eqref{eq:chain} that:
\begin{align}
    I(X;Y) &= H(Y) - H(Y|X) \notag \\
           &= H(Y) - [ H(X,Y) - H(X) ] \notag \\
           &= H(X) + H(Y) - H(X,Y) \label{eq:chainmutual}
\end{align}

Thus, three equations for Mutual Information involving entropy are 
\eqref{eq:mutual1}, \eqref{eq:mutual2} and \eqref{eq:chainmutual}.

This can be verified intuitively in Figure \ref{fig:mutual}.


\item
Since entropy only considers the distribution, and not the values themselves, it is known that we can give any
unambiguous labels to the elements of $\mathcal{X}$ and achieve the same entropy, as long as the distribution includes the
same probabilities.

The negation $f(x) = -x$ is a mapping from $x$ to $0-x$, that is, swapping $x_i \in \mathcal{X}$ with $x_{-i} \in \mathcal{X}$, $\forall i = 0, 1, 2...$.

This means that $p_{-X}$ is $p_{X}$ mirrored about $\mu = 0$. The image of the functions is the same.
The only difference that occurs when $X$ is symmetrical is that $p_{-X}(x) = p_{X}(x)$. This difference is inconsequential, since either way the (unordered) set of probabilities is the same, hence $H(Y|X) = H(Y|-X)$. A rigorous proof follows.

Since negation is a $1:1$ function, $p_{-X}(-x) = p_X(x)$.

\begin{align*}
 H(Y|-X) &= \mysum{x \in \mathcal{X}} p_{-X}(-x) H(Y|X=-x) \\
         &= \mysum{x \in \mathcal{X}} p_{X}(x) H(Y|X=x) \\
         &= H(Y| X) \qed
\end{align*}

This can be confirmed using the result for question (c).

\item
Intuitively, there is no ambiguity when going from $X$ to $T(X)$, meaning the values for the probabilities should be the same,

A function $f$ is invertible when it is \emph{bijective}:

\[f : \mathcal{X} \rightarrow \mathcal{X^{\prime}}, \forall x,y \in \mathcal{X}, f(x) = f(y) \implies x=y\]

Let $T$ represent the random variable given by $T(X)$. Let $t = T(x)$, then each $t$ is mapped to by exactly one $x$, and each $x$ maps to exactly one $t$. This means that the
probability $p_T(T(x)) = p_X(x)$. Hence, the sets of probabilities $p(T,Y) = p(X,Y)$ and $p(Y|X) = p(Y|T(X))$. Due to the
commutativity of addition,

\begin{align*}
    H(Y|X) &= \mysum{(x,y) \in \mathcal{X}\times\mathcal{Y}} p(x,y) \log p(y|x) \\
           &= \mysum{(t,y) \in \mathcal{T}\times\mathcal{Y}} p(t,y) \log p(y|t) \\
           &= H(Y|T(X))
\end{align*}


\item
If $T$ is not invertible, then it may still be invertible in some restricted domain (like $\sin(\cdot)$ for example), so it depends on $\mathcal{X}$. 
In the case where $T$ is invertible over $\mathcal{X}$, we can consider the answer to (c), that is $H(Y|X) = H(Y|T(X))$.

In the case where $T$ is not invertible over $\mathcal{X}$, we let $t = T(x)$. Then let $T^{-1}(t) = \{x : t = T(x), t \in \mathcal{T}, x \in \mathcal{X}\}$,$\exists t\in\mathcal{T}, |T^{-1}| > 1$.


From this,
$p_T(t) \ge p_X(x) \forall x:t=T(x)$, $p(y, t) = \mysum{x:t=T(x)} p(y, x)$ and $p(y|t) = \mysum{x:t=T(x)}p(y|x)$.

such that

\[
    \mysum{x:t=T(x)}p(x,y) \log p(y|x) \le \mysum{x:t=T(x)} p(x,y) \log p(y|t) = p(t,y) \log p(y|t)
\]


Hence, 

\begin{align*}
    H(Y|X) &= \mysum{(x,y) \in \mathcal{X}\times\mathcal{Y}} p(x,y) \log p(y|x) \\
           &= \mysum{t\in\mathcal{T}}\mysum{x:t=T(x)} p(x,y) \log p(y|x) \\
           &< \mysum{t\in\mathcal{T}} p(t, y) \log p(y|t) \\
           &= H(Y|T(X))
\end{align*}

So the general relationship for a non-invertible function $T(X)$ is $H(Y|X) \ge H(Y|T(X))$, with equality only when $T(X)$ is invertible for $\mathcal{X}$.

\item The relationship $H(T(X)) \le H(X)$ can be shown like so.

Let $T$ represent the random variable $T(X)$, $t = T(x)$ in the alphabet $\mathcal{T}$, then $p_T(t) = \mysum{x:t=T(x)}p_X(x)$ (the sum of all probabilities of $x$'s that map to $t$ by applying $T$).
Then $p_T(t) \ge p_X(x) \forall x:t=T(x)$, with equality only when there is a $1:1$ mapping (an invertible function will always have such $1:1$ mappings).

Now consider any set of $x$'s that maps to a single $t$. For this set,

\[
    \mysum{x:t=T(x)}p_X(x) \log p_X(x) \le \mysum{x:t=T(x)} p_X(x) \log p_T(t) = p_T(t) \log p_T(t)
\]

Using this,

\begin{align*}
    H(X) &= -\mysum{x\in\mathcal{X}} p_X(x) \log p_X(x) \\
         &= -\mysum{t\in\mathcal{T}} \mysum{x:t=T(x)} p_X(x) \log p_X(x) \\
         &\ge \mysum{t\in\mathcal{T})} p_T(x) \log p_T(t) \\
         &= H(T(X)) \qed
\end{align*}
With equality only if $T(X)$ is invertible. This makes sense intuitively, as a function on a random variable essentially relabels the events and possibly reorders the probability distribution, but entropy is only concerned with the probabilities (not values or labels) and uses a commutative operation (hence order doesn't matter, as long as the set of probabilities is the same).

If $T(X)$ is not invertible (for the range of $\mathcal{X}$ with non-zero probability), then that means there are at least two values of $x$ which map to the same $t$. In a sense, it collapses two or more events to be represented by one event which could be either (but we don't care which). This means we have less possible outcomes of $T$, so we have less uncertainty.

\item
If $Y=X+Z$, we can show that $H(Y|Z)=H(X|Z)$ as follows
\begin{align*}
    H(Y|Z) &= H(X + Z|Z)\\
           &= \mysum{z\in\mathcal{Z}} p_Z(z) H(X + z | Z=z)\\
           &= \mysum{z\in\mathcal{Z}} p_Z(z) H(X|Z) \qquad\text{\emph(From (c), as $T(X)=X+z$ is invertible)}\\
           &= H(X|Z)
\end{align*}

Since $I(X;Z) = H(X) - H(X|Z)$, then from the above result $H(Y|Z) = H(X|Z) = H(X) - I(X;Z)$.
Solving for $H(Y|Z) - H(X) = -I(X;Z) = 0$, we can say $H(Y|Z) = H(X)$ if $I(X;Z)=0$, which can only happen if $X$ and $Z$ are independent.
