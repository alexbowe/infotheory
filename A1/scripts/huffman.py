def build_tree(prob_symbol_pairs):
    import heapq as hq
    huff_tree = list(prob_symbol_pairs)
    hq.heapify(huff_tree)

    while len(huff_tree) > 1:
        left, right = hq.heappop(huff_tree), hq.heappop(huff_tree)
        parent = (left[0] + right[0], left, right)
        hq.heappush(huff_tree, parent)
    
    return huff_tree[0]

def codebook(huff_tree, prefix = ''):
    codebook = []
    # the length equals 2 when one element is left
    if len(huff_tree) == 2:
        return codebook + [(huff_tree[1], prefix)]
    # left child
    codebook += huffman_code(huff_tree[1], prefix + '0')
    # right child
    codebook += huffman_code(huff_tree[2], prefix + '1')
    return codebook
