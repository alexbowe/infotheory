\item The typical set $A_\epsilon^{(n)}$ with respect to a continuous distribution $f_X(x)$ is defined as:
\begin{mydef}[Typical Set] 
    \[
    A_\epsilon^{(n)} = \{(x_1, x_2, ..., x_n) \in S^n : | -\frac{1}{n}\log f(x_1, x_2,..., x_n) - h(f_X) | \le \epsilon \}
    \]
    
    where $S^n$ is the support set (elements with positive probability) of $(x_1, x_2, ..., x_n)$,
    $f(x_1, x_2, ..., x_n) = \displaystyle\prod\limits_{i=1}^n f_X(f_X)$, $\epsilon > 0$ and $n \in \mathbb{N}$.

    Essentially, this is the set of vectors that have an \emph{empirical} (witnessed) entropy within
    $\epsilon$ bits (or nats, say) of the \emph{actual} entropy of the distribution.
\end{mydef}


\item
\begin{enumerate}
    \item
        \begin{align*}
            h(f_X) &= -\int_0^\infty \lambda e^{-\lambda x} \ln \lambda e^{-\lambda x}\, \mathrm{d}x \\
                   &= -\int_0^\infty \left( \lambda e^{-\lambda x} \ln \lambda + e^{-\lambda x} \ln e^{-\lambda x}\right)\, \mathrm{d}x \\
                   &= -\ln \lambda \int_0^\infty \lambda e^{-\lambda x}\,\mathrm{d}x - \int_0^\infty (-\lambda x) \lambda e^{-\lambda x }\, \mathrm{d}x  \\
                   &= -\ln \lambda \int_0^\infty \lambda e^{-\lambda x}\,\mathrm{d}x + \lambda \int_0^\infty x \lambda e^{-\lambda x }\, \mathrm{d}x \\
        \end{align*}

        since
        \begin{align*}
            \int_0^\infty \lambda e^{-\lambda x}\,\mathrm{d}x
                &= \lim_{b \to \infty} - e^{-\lambda b} + e^{-\lambda 0} \\
                &= \lim_{b \to \infty} - e^{-\lambda b} + 1 \\
                &= 1
        \end{align*}

        and
        \begin{align*}
            \int_0^\infty x e^{-\lambda x}\,\mathrm{d}x
                &= \left[-x e^{-\lambda x} + \int e^{- \lambda x}\,\mathrm{d}x\right]_0^\infty \\
                &= \left[-x e^{-\lambda x} - \frac{1}{\lambda} e^{-\lambda x} + c\right]_0^\infty\\
                &= \lim_{b \to \infty} (-b - \frac{1}{\lambda})e^{-\lambda b} + \frac{1}{\lambda} \\
                &= \frac{1}{\lambda}
        \end{align*}

        then
        \begin{align*}
            h(f_X) &= -\ln \lambda \int_0^\infty \lambda e^{-\lambda x}\,\mathrm{d}x + \lambda \int_0^\infty x \lambda e^{-\lambda x }\, \mathrm{d}x \\
                &= -\ln \lambda + 1
        \end{align*}

    
    \item
        The plot of $h(f_X) = -\ln \lambda + 1$ is shown below:
        \DefFig{fig:lam}{differential/lambdagraph}{0.9}
        {Graph of $h(f_X) = -\ln \lambda + 1$.}

        This demonstrates that differential entropy can be negative, unlike discrete entropy (which is always zero or positive). 

    
    \item
        From the definition of a typical set, a vector $\textbf{x}^n = (x_1, x_2, ..., x_n)$ is in $A_\epsilon^{(n)}$ if it satisfies this (in)equality:

        \[
            h(f_X) - \epsilon \le - \frac{1}{n} \ln f(x_1, x_2, ..., x_n) \le h(f_X) + \epsilon
        \]

        where $f(x_1, x_2, ..., x_n) = \displaystyle\prod\limits_{i=1}^n f_X(x_i)$.

        For $f_X$ this gives us

        \[
            1 - \ln \lambda - \epsilon \le -\frac{1}{n} \ln \displaystyle\prod\limits_{i=1}^n \lambda e^{-\lambda x_i} \le
            1 - \ln \lambda + \epsilon
        \]

        Rearranging:

        \[
            \frac{n(1 - \epsilon)}{\lambda} \le \displaystyle\sum\limits_{i=1}^n x_i \le
            \frac{n(1 + \epsilon)}{\lambda}
        \]

    
    \item
        Expanding the inequality from (c) gave us a linear equation of multiple variables $x_1, x_2, ..., x_n$, which
        means that the typical set will be a line, plane, or hyperplane in $n$ dimensions. The terms ``line'' and so on
        are used loosely here, as the two inequalities form a thin region (between two lines, planes, hyperplanes...)
        that the points fall within. In this case, the domain is restricted to $x_i \ge 0, \forall i=1, 2, ..., n$.

        As for the Gaussian function $\phi(x) = \frac{1}{\sqrt{2\pi\sigma^2}}e^{\frac{-x^2}{2\sigma^2}}$, having entropy
        $h(\phi) = \ln \sigma\sqrt{2\pi e}$, the inequality from the definition of the typical set is:

        \[
            \ln \sigma\sqrt{2\pi e} - \epsilon \le
                - \frac{1}{n} \ln \displaystyle\prod\limits_{i=1}^n \frac{1}{\sqrt{2\pi\sigma^2}}e^{\frac{-x_i^2}{2\sigma^2}}
                \le \ln\sigma\sqrt{2\pi e} + \epsilon 
        \]


        Rearranging:

        \[
            2n\sigma^2(\ln 2 \pi \sigma^2 + 1 - \epsilon)
            \le \displaystyle\sum\limits_{i=1}^n x_i^2 
            \le
            2n\sigma^2(\ln 2 \pi \sigma^2 + 1 + \epsilon)
        \]

        Functions of the form $\sum x^2 = r$ produce a circle, sphere, and hypersphere in $n$ dimensions, with radius $r$.
        In this case, the inequality forms two circles, spheres, or hyperspheres, with different radii. This forms a region
        that resembles a hollowed (hyper)sphere (or cross-section of such a shape, depending on the dimension).
        Unlike the exponential distribution, the domain is over all reals.

        It is worth noting that the points of axis intercection in the first function, and the size of the radius, scale
        upward as $n$ increases.

        For $n = 2$, our functions are:

        \begin{align}
            \frac{2(1 - \epsilon)}{\lambda} &\le x_1 + x_2 \le \frac{2(1 + \epsilon)}{\lambda} \\
            4\sigma^2(\ln 2 \pi \sigma^2 + 1 - \epsilon) &\le x_1^2 + x_2^2 \le 4\sigma^2(\ln 2 \pi \sigma^2 + 1 + \epsilon)
        \end{align}

        Which are respectively plotted in Figure \ref{fig:reg1} and Figure \ref{fig:reg2}
        below (please excuse the lack of nicely typeset maths, my drawing/plotting tools do not
        have all the features I would like them to have, and I am too
        short on time to use \texttt{gnuplot} or a similar package).

        \DefFig{fig:reg1}{differential/region1}{0.8}
            {Shape of typical set of an Exponential distribution.}

        \DefFig{fig:reg2}{differential/region2}{0.8}
            {Shape of typical set of a Gaussian distribution.}

        \clearpage 
    \item
        Since there are $10^3$ possible three-digit combinations, three
        digits of accuracy implies $n = \lceil \log 10^3\rceil = 10$ bits of accuracy.

        Due the quantization of a continuous random variable, it takes $h(X) + n$ bits on average to
        describe an answer to $n$-bit accuracy. Hence it takes $h_2(X_i) + n = 10 + \log e (1 - \ln \lambda) = 10 + \log e - \log \lambda$ bits to
        describe $X_i$ to three digit accuracy.

        $h(aX) = h(X) + \log |a|$. $Y_i = -2X_i$, so $h(Y_i) = h(-2X_i) = h(X_i) + \log 2 = h(X_i) + 1$.
        Hence, to describe $Y_i$ to three digit accuracy, we require $11 + \log e - \log \lambda$ bits.
        
\end{enumerate}
